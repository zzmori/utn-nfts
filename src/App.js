import logo from './logo.svg';
import './App.css';

import React from 'react';

import {Web3Context, provider} from './context/Web3Context';
import Contract from './components/Contract.js'

function App() {
  return (
    <Web3Context.Provider value={provider}>
      <div className="App">
        <Contract />
      </div>
    </Web3Context.Provider>
  );
}

export default App;
