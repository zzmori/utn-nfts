import React from 'react'

function ipfsUriToGateway(uri) {
    if (uri.startsWith("ipfs://")) {
        uri = uri.slice(7)
        uri = `https://gateway.pinata.cloud/ipfs/${uri}`
    }
    return uri
}

export default function Card({contract, id}) {
    const [metadata, setMetadata] = React.useState(null)

    React.useEffect(() => {
        contract.tokenURI(id).then(uri => {
            fetch(ipfsUriToGateway(uri)).then(data => {
                data.json().then(j =>
                    setMetadata(j)
                )
            })
        })
    }, [id])
    return <div style={{
        display: "flex",
        border: "1px solid black"
    }}>
        <h4>token #{id}</h4>
        <img src={metadata?ipfsUriToGateway(metadata.image):"#"} />
    </div>
}