import React from "react";
import { Web3Context } from "../context/Web3Context";
import { ethers } from "ethers";

import Card from "./Card";

const ABI = [
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "tokenId",
				"type": "uint256"
			}
		],
		"name": "tokenURI",
		"outputs": [
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [],
		"name": "totalSupply",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	}
]

export default function Contract () {
    const context = Web3Context
    const contract = React.useMemo(
        () => 
        new ethers.Contract(
            "0xBC4CA0EdA7647A8aB7C2061c2E118A18a936f13D",
            ABI,
            context._currentValue
        ), [context])

    const [totalSupply, setTotalSupply] = React.useState(0)
    const [page, setPage] = React.useState(0)
    
    React.useEffect(() => {
        contract.totalSupply().then(ts => {
            console.log(ts)
            setTotalSupply(ts.toNumber())
        })
    }, [contract])

    let cards = []
    for (let i = page * 10; i < page * 10 + 10 && i < totalSupply; i++) {
        cards.push(
            <Card contract={contract} id={i} key={i}/>
        )
    }

    return (
        <>
            {cards}
        </>
    )
}